#!/bin/bash
echo
#更改wo以及root密码
su root
echo "修改用户 wo 的密码，请注意！"
sleep 0.5
sudo passwd wo
echo "--------------------------------------"
echo
echo "修改 root 用户密码，一定要注意啊！！！"
sleep 1
sudo passwd root
sleep 1


echo "======================================="
#将普通用户wo添加到sudo组
sudo usermod -aG sudo wo
echo "---- 已将普通用户wo 添加到sudo组。-----"
echo "======================================="
echo
sleep 1

#选择是否开始执行 ./installapp.sh 
echo -n "是否开始执行下一脚本(是:1 否:2):"
read res
if [ $res == 1 ]
then
	./installapp.sh
else
	echo "请查阅并修改后，再执行 ./installapp.sh "
fi
exit 0
