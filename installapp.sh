#!/bin/bash
echo
#将大写键与左Ctrl互换键位
echo -n "是否将大写键与左Ctrl键位互换(是:1  否:2)："
read sure
if [ $sure == 1 ]
then
	#大写键在42行，默认键值为66，将其改为37
	sudo sed -i '38,48s/66/37/' /usr/share/X11/xkb/keycodes/evdev
	#左Ctrl键在69行，默认键值为37，将其换为66
	sudo sed -i '64,74s/37/66/' /usr/share/X11/xkb/keycodes/evdev
	echo "------- 键位已互换 -------"
else
	echo "------- 键位未更改 -------" 
fi


echo "即将安装软件，去休息一下吧......"
sleep 5
#安装软件
sudo apt -y remove nano
sudo apt update && sudo apt -y upgrade
sudo apt -y install curl \
	git \
	build-essential \
	gcc \
	fish \
	code \
	google-chrome-stable \
	neovim \
	neofetch \
	variety \
	alacritty \
	brightnessctl \
	tmux \
	tree \
	v2raylui \
	telegram-desktop \
	aria2
sudo apt clean

echo
echo "************************************"
#将 fish shell 更改为 wo 的默认Shell
sudo chsh wo -s /usr/bin/fish
echo " 已将fish shell设置为wo的默认Shell"
echo "************************************"
echo "  万事俱备，只欠重启（完结撒花~）"
echo "  <(*￣▽￣*)/   Have a good time! "
echo "************************************"
echo
exit 0

