# debset

#### 介绍
安装[铜豌豆操作系统](https://www.atzlinux.com/)后的Shell配置脚本。
*友情链接:[铜豌豆Linux官方仓库](https://gitee.com/atzlinux/debian-cn)*

#### 使用说明

1.  以root用户执行脚本。
2.  进入脚本所在目录，执行./first.sh 根据提示输入数字 1 或者 2 
3.  **注：建议先看一下脚本内容，根据个人所需进行修改！**

#### 脚本内容

1.  first.sh:
    + 修改 root 和 wo 的密码
    + 将用户wo加入sudo组
2.  installapp.sh:
    + 将大写键与左Ctrl互换键位
    + 更新软件源
    + 安装一系列软件(建议根据个人所需进行修改！)
    + 将fish shell设置为 wo 的默认Shell
